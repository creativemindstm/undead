#include "game_scene.hpp"

#include "main_scene.hpp"
#include "../ud.hpp"

ud::game_scene::game_scene ( ud::window& window )
:   window ( window ), 

     parallax_background ( {
        "../assets/images/BackMountain.png",
        "../assets/images/BackMountain2.png",
        "../assets/images/BackMountain3.png"
    } )
{
    {
        parallax_background.get_layer(0).sprite->setPosition ( 0, -600 );
        parallax_background.get_layer(0).offset = { 1.5, 1.5 };
        parallax_background.get_layer(1).sprite->setPosition ( 0, -300 );
        parallax_background.get_layer(1).offset = { 1.2, 1.2 };
        parallax_background.get_layer(2).sprite->setPosition ( 0, -100 );
    }
    {
        walking_lane.set ( "../assets/images/WalkingLane.png" );
    
        const auto tex_offset_y { 1080 / 2 + 20 };
    
        walking_lane.set_frame_size ( { 0, tex_offset_y } );
        walking_lane->setPosition ( 0, 70 );
    }
    
    {/*
        player.setRadius ( 50.0f );
        player.setPointCount ( 500 );
        player.setFillColor ( sf::Color::Blue );
        player.setPosition ( 0, walking_lane.getPosition().y - player.getLocalBounds().height );
    */}
    
    {
        camera.setCenter ( 0, 0 );
        camera.setSize ( sf::Vector2f { window->getSize () } );
    }
}

ud::game_scene::~game_scene ()
{
}

void ud::game_scene::process_event 
( ud::window& window, const sf::Event& event )
{
    if ( event.type == sf::Event::Resized )
        camera.setSize ( sf::Vector2f { window->getSize () } );
    
    if ( event.type == sf::Event::KeyPressed )
    {
        if ( is_direction_key ( event.key.code ) )
        {
            auto displacement {
                direction_vectors.at ( get_direction ( event.key.code ) ) * 10.0f };
                
            player.walk ( displacement );
            camera.move ( displacement );
        }
        
        switch ( event.key.code )
        {
            case sf::Keyboard::M:
                set_scene <ud::main_scene> ( window );
        }
    }
}

void ud::game_scene::update ( ud::window& window )
{
}

void ud::game_scene::draw ( ud::window& window )
{    
    window->clear ( sf::Color::Cyan );
    window->setView ( camera );
    parallax_background.draw ( window );
    walking_lane.draw ( window );
    player.draw ( window );
}
