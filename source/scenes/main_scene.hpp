#pragma once

#include <SFML/Graphics.hpp>

#include "../scene.hpp"
#include "../window.hpp"
#include "../objects/button.hpp"

namespace ud
{
class main_scene : public scene
{
public:
    main_scene ( ud::window& );
    
    void process_event ( ud::window&, const sf::Event& ) override;
    void draw ( ud::window& ) override;
    
private:
    class button : public ud::button
    {
    public:
        button ( ud::window&, std::string, int, int );
        
    private:
        sf::Font font;
    };
    
    ud::window& window;
    sf::Texture background_texture;
    sf::Sprite background;
    button start_button, quit_button;
};
}
