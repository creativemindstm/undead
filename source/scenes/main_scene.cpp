#include "main_scene.hpp"
#include "game_scene.hpp"

ud::main_scene::button::button ( ud::window& window, std::string str, int x, int y )
:   ud::button ( window )
{
    font.loadFromFile ( "../assets/fonts/animeace.ttf" );
    text.setString ( str );
    text.setFont ( font );
    text.setCharacterSize ( 40 );
    
    set_position ( { ( window->getSize().x / 100 ) * x,
            ( window->getSize().y / 100 ) * y } );
}

ud::main_scene::main_scene ( ud::window& window )
:   window ( window ),
    start_button ( window, "start", 20, 40 ),
    quit_button ( window, "quit", 20, 60 )
{
    background_texture.loadFromFile ( "../assets/images/MainScreen.png" );
    background.setTexture ( background_texture );
    
    background.scale ( window->getSize().x / background.getLocalBounds().width,
                       window->getSize().y / background.getLocalBounds().height );
    
    start_button.on_hover_text_color = sf::Color::Green;
    quit_button.on_hover_text_color = sf::Color::Red;
    
    start_button.on_click_callback = [this, &window] () { set_scene <game_scene> ( window ); };
    //quit_button.on_click_callback = [this] () { this->window.close (); };
}

void ud::main_scene::process_event ( ud::window& window, const sf::Event& event )
{
    start_button.update ( window, event );
    quit_button.update ( window, event );
}

void ud::main_scene::draw ( ud::window& window )
{
    window->setView ( window->getDefaultView () );
    window->draw ( background );
    start_button.draw ( window );
    quit_button.draw ( window );
}
