#pragma once

#include <vector>

#include "../scene.hpp"
#include "../window.hpp"
#include "../objects/player.hpp"
#include "../objects/parallax_background.hpp"
#include "../components/sprite.hpp"

namespace ud
{
class game_scene : public scene
{
public:
    game_scene ( ud::window& );
    ~game_scene ();
    
    void process_event ( ud::window&, const sf::Event& ) override;
    void update ( ud::window& ) override;
    void draw ( ud::window& ) override;
    
private:
    ud::window& window;
    ud::sprite s1, s2;
    ud::parallax_background parallax_background;
    std::vector <sprite> static_background_layers;
    ud::sprite walking_lane;
    ud::player player;
    sf::View camera;
};
}
