#pragma once

#include <SFML/Graphics.hpp>

namespace ud
{
enum class direction
{
    left, right, up, down
};

inline bool is_direction_key ( sf::Keyboard::Key key )
{ 
    return key == sf::Keyboard::Key::Left
        || key == sf::Keyboard::Key::Right
        || key == sf::Keyboard::Key::Up 
        || key == sf::Keyboard::Key::Down; 
}

inline direction get_direction ( sf::Keyboard::Key key )
{
    switch ( key )
    {
        case sf::Keyboard::Key::Left: return direction::left; break;
        case sf::Keyboard::Key::Right: return direction::right; break;
        case sf::Keyboard::Key::Up: return direction::up; break;
        case sf::Keyboard::Key::Down: return direction::down; break;
        default: throw std::runtime_error ( "key is not a direction key" );
    }
}

static const std::map < direction, sf::Vector2f > direction_vectors
{
    { direction::left,  { -1,  0 } },
    { direction::right, {  1,  0 } },
    { direction::up,    {  0, -1 } },
    { direction::down,  {  0,  1 } }
};

}
