#include "editor_window.hpp"

#include "../scene.hpp"

ud::editor_window::editor_window ()
:   window ( { 400, 400 }, "Undead-Editor" )
{
}

void ud::editor_window::update()
{
    sf::Event event;
    while ( window.pollEvent(event) );
    
    window.clear();
    window.display ();
}
