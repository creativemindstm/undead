#pragma once

#include <SFML/Graphics.hpp>

namespace ud
{
class editor_window
{
public:
    editor_window ();
    
    void update ();
    
private:
    sf::RenderWindow window;
};
}
