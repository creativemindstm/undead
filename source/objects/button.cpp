#include "button.hpp"
#include <iostream>
#include "../window.hpp"

ud::button::button ( ud::window& window )
{
}

ud::button::button ( ud::window& window, std::string string, const sf::Font& font, int size )
: text ( string, font, size )
{
}

void ud::button::set_position ( sf::Vector2f pos )
{
    text.setPosition ( pos.x, pos.y );
}

void ud::button::draw ( ud::window& window )
{
   window->draw ( text );
}

sf::FloatRect ud::button::get_global_bounds() const
{
    return text.getGlobalBounds();
}

void ud::button::on_click ()
{
    if ( on_click_callback )
        on_click_callback ();
}

void ud::button::on_mouse_over()
{
    text.setFillColor ( on_hover_text_color );
}

void ud::button::on_mouse_out()
{
    text.setFillColor ( text_color );
}

/*
void ud::button::update()
{
    sf::Vector2f mouse_pos { Input::get().get_mouse_position() };
    
    decltype ( callbacks )::iterator callback_i;
    
    if ( text.getGlobalBounds().contains(mouse_pos) )
    {
        if ( sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) )
            call_if_found ( click );
        
        call_if_found ( mouse_over ), mouse_was_over = true;
    }
    else {
        if ( mouse_was_over ) 
            call_if_found ( mouse_out ), mouse_was_over = false;
    }
            
}*/
