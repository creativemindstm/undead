#pragma once

#include <vector>

#include <SFML/Graphics.hpp>

#include "../components/sprite.hpp"
#include "../components/drawable.hpp"
#include "../components/editor_interface.hpp"

namespace ud
{
class parallax_background : drawable, editor_interface
{
public:
    struct layer
    {
        ud::sprite sprite;
        sf::Vector2f offset { 1, 1 };
    };
    
    parallax_background ( const std::vector <std::string>& layers );
    
    void draw ( ud::window& );
    layer& get_layer ( int index ) { return this->layers [ index ]; }
    
private:
    std::vector <layer> layers;
};
}
