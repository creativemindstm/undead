#pragma once

#include <unordered_map>
#include <string>
#include <memory>

#include <SFML/Graphics.hpp>

#include "../ud.hpp"
#include "../window.hpp"

#include "../components/drawable.hpp"
#include "../components/editor_interface.hpp"

namespace ud
{
class player : 
public drawable, public editor_interface
{
public:
    player ();
    
    void walk ( sf::Vector2f );
    void draw ( ud::window& );
    
private:
};
}
