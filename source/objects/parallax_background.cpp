#include "parallax_background.hpp"

ud::parallax_background::parallax_background
( const std::vector<std::string>& file_paths )
{
    layers.resize ( file_paths.size () );
    
    int index { 0 };
    for ( const auto& file_path : file_paths )
        layers[index].sprite.set ( file_path ), ++index;
}

void ud::parallax_background::draw 
( ud::window& window )
{
    auto view { window->getView() };
    for ( auto& layer : layers )
    {
        sf::View o_view;
        
        sf::Vector2f center { 
            view.getCenter().x * layer.offset.x,
            view.getCenter().y * layer.offset.y
        };
        
        o_view.setCenter ( center );
        window->setView ( o_view );
        layer.sprite.draw ( window );
    }
    window->setView ( view );
}
