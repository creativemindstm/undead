#pragma once

#include <map>
#include <functional>

#include <SFML/Graphics.hpp>

#include "../components/user_interface.hpp"
#include "../components/drawable.hpp"

namespace ud
{
class button : public user_interface, public drawable
{
public:
    button ( ud::window& );
    button ( ud::window&, std::string, const sf::Font&, int );
    
    void draw ( ud::window& ) override;
    sf::FloatRect get_global_bounds () const override;
    
    void on_click() override;
    void on_mouse_over() override;
    void on_mouse_out() override;
    
    void set_position ( sf::Vector2f );
    auto& get_text () { return text; };
    
    sf::Color text_color { sf::Color::White };
    sf::Color on_hover_text_color { text_color };
    
    std::function < void () > on_click_callback;
    
protected:
    sf::Text text;
};
}
