#include <memory>

#include "window.hpp"
#include "scenes/main_scene.hpp"
#include "editor/editor_window.hpp"

int main () 
{
    ud::window window { { 800, 400 }, "Undead" };
    ud::editor_window editor {};
    ud::scene::set_scene <ud::main_scene> ( window );
    
    while ( window.update (), window )
    {
        editor.update ();
    }
}
