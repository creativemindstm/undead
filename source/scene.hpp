#pragma once

#include <map>
#include <typeinfo>
#include <typeindex>
#include <memory>

#include <SFML/Graphics.hpp>

#include "window.hpp"

namespace ud
{

class scene
{
public:
    static scene& get_current_scene () { return *current_scene; }
    
    template < class Scene, class ...Args >
    static void set_scene ( Args&& ...args )
    { current_scene.reset ( new Scene { std::forward <Args> ( args )... } ); }
    
    virtual ~scene () {};
    
    virtual void process_event ( ud::window&, const sf::Event& ) = 0;
    virtual void update ( ud::window& ) {};
    virtual void draw ( ud::window& ) = 0;
    
private:
    static inline std::unique_ptr <scene> current_scene;
};

}
