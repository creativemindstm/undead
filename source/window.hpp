#pragma once

#include <vector>
#include <map>
#include <functional>
#include <type_traits>

#include <SFML/Graphics.hpp>

namespace ud
{
    
class scene;

class window
{
public:    
    using event_callback = std::function < void ( sf::Event& ) >;
    
    window ( sf::VideoMode, std::string );
    
    sf::RenderWindow* operator -> ();
    operator bool () const;
    
    void update ();
    
private:
    void process_events ();
    void main_loop ();
    
    sf::RenderWindow object;
    
    bool is_open;
};

}
