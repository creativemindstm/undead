#include "sprite.hpp"

ud::sprite::sprite ( std::string file_path )
{
    set ( file_path );
}

void ud::sprite::draw ( ud::window& window ) const
{
    window->draw ( object );
}

void ud::sprite::set ( std::string file_path )
{
    texture.loadFromFile ( file_path );
    object.setTexture ( texture );
}

void ud::sprite::set_frame_size ( sf::Vector2f size )
{
    object.setTextureRect ( { 
        size.x, size.y, 
        texture.getSize().x - size.x,
        texture.getSize().y - size.y
    } );
}

void ud::sprite::move_frame ( sf::Vector2f offset )
{
    auto rect_position { object.getTextureRect().getPosition() };
    auto rect_size { object.getTextureRect().getSize () };
    
    object.setTextureRect ( { 
        rect_position.x + offset.x,
        rect_position.y + offset.y,
        rect_size.x + offset.x,
        rect_size.y + offset.y
    } );
}
