#pragma once

#include <map>
#include <vector>

#include <SFML/Graphics.hpp>

#include "../ud.hpp"
#include "../window.hpp"

namespace ud
{
class sprite
{
public:
    sprite () {}
    sprite ( std::string file_path );
    
    sprite ( const sprite& ) = default;
    sprite ( sprite&& ) = default;
    
    sf::Sprite* operator -> () { return &object; }
    
    void draw ( ud::window& ) const;
    
    void set ( std::string );
    void set_frame_size ( sf::Vector2f );
    void move_frame ( sf::Vector2f );
    
private:
    sf::Texture texture;
    sf::Sprite object;
};
}
