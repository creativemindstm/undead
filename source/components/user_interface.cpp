#include "user_interface.hpp"

void ud::user_interface::update ( ud::window& window, sf::Event event )
{
    if ( event.type == sf::Event::MouseButtonPressed )
    {
        sf::Vector2i pixel { event.mouseButton.x, event.mouseButton.y };
            
        if ( get_global_bounds().contains ( window->mapPixelToCoords ( pixel ) ) )
            on_click();
    };
    
    if ( event.type == sf::Event::MouseMoved )
    {
        auto pixel { sf::Vector2i ( static_cast<float>(event.mouseMove.x), 
                static_cast<float>(event.mouseMove.y ) ) };
                
        if ( get_global_bounds().contains ( window->mapPixelToCoords ( pixel ) ) )
            on_mouse_over (), mouse_was_over = true;
        else on_mouse_out (); mouse_was_over = false;
    };
}

