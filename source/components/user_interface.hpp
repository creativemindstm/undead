#pragma once

#include "../window.hpp"

namespace ud
{
class user_interface
{
public:
    void update ( ud::window&, sf::Event );
    
protected:
    virtual sf::FloatRect get_global_bounds () const = 0;
    virtual void on_click () {};
    virtual void on_mouse_over () {};
    virtual void on_mouse_out () {};
    bool mouse_was_over { false };
};
};
