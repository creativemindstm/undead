#pragma once

#include <vector>

#include "../window.hpp"

namespace ud
{
class drawable
{
public:
    virtual void draw ( ud::window& ) = 0;
};
}
