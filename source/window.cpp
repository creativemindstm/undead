#include "window.hpp"

#include "scene.hpp"

ud::window::window ( sf::VideoMode video_mode, std::string title )
:   object ( video_mode, title ),
    is_open ( true )
{
}

sf::RenderWindow* ud::window::operator->()
{
    return &object;
}

ud::window::operator bool() const
{
    return is_open;
}

void ud::window::process_events ()
{
    sf::Event event;
    
    while ( object.pollEvent ( event ) )
    {
        switch ( event.type )
        {
            case sf::Event::Closed: is_open = false; return;
            default: scene::get_current_scene().process_event ( *this, event );
        }
    }
}

void ud::window::update ()
{
    process_events();
    object.clear ();
    scene::get_current_scene().draw (*this);
    object.display ();
}
