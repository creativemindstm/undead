cmake_minimum_required ( VERSION 3.16 )

set ( CMAKE_CXX_STANDARD 17 )

add_subdirectory ( SFML )

project ( UNDEAD )

file ( GLOB_RECURSE source_files "${PROJECT_SOURCE_DIR}/source/*.cpp" )

add_executable ( UNDEAD ${source_files} )

target_include_directories ( UNDEAD PRIVATE "SFML/include" )
target_link_libraries ( UNDEAD PRIVATE sfml-graphics )
